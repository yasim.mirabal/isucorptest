using System.Collections.Generic;
using Reservation.Models;
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using JsonNet.PrivateSettersContractResolvers;
using Reservation.Data;

namespace Reservation
{
    public class Seeder
    {
        public static void Seedit(string jsonData, IServiceProvider serviceProvider)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = new PrivateSetterContractResolver() };
            List<Place> places = JsonConvert.DeserializeObject<List<Place>>(jsonData, settings);
            using (
            var serviceScope = serviceProvider
                .GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope
                            .ServiceProvider.GetService<DataContext>();
                if (!context.Places.Any())
                {
                context.AddRange(places);
                context.SaveChanges();
                }
            }
        }
    }
}