using Microsoft.EntityFrameworkCore;
using Reservation.Models;

namespace Reservation.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Reserve> Reserves {get; set;}
        public DbSet<Place> Places {get; set;}
    }
}