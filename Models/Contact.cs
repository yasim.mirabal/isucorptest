using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Reservation.Models
{
    public class Contact
    {
        public int Id {get; set;}
        public string FullName {get; set;} = "Yasim";
        public System.DateTime BirthDate {get; set;} = new System.DateTime(1982, 9, 10);
        public ContactType Type {get; set;} = ContactType.ContactType1;
        public string Phone {get; set;} = "+5353584261";
        public string Description {get; set;} = "A short description of you.";
        [JsonIgnore]
        public List<Reserve> Reserves {get; set;}
    }
}