using System;

namespace Reservation.Models
{
    public class Reserve
    {
        public int Id {get; set;}
        public DateTime DateofReserve {get; set;}
        public Contact Contact {get; set;}
        public Place Place {get; set;}
    }
}