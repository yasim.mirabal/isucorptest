using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Reservation.Models
{
    public class Place
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public float Rating {get; set;}
        public bool Favorite {get; set;}
        public string Image {get; set;}
        [JsonIgnore]
        public List<Reserve> Reserves {get; set;}
    }
}