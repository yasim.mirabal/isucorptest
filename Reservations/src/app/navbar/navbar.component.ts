import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  btn: string;

  // Navbar and header buttons Navigation manager.
  constructor(private router : Router) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
      var splitted = window.location.href.split("/");
      this.btn = splitted[3];
      }
    })
   }

  // Navigation assistant.
  ngOnInit(): void {
    var splitted = window.location.href.split("/");
    this.btn = splitted[3];

  }
}
