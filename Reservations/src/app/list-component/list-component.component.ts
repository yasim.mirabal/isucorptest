import { ReservesService } from './../reserves.service';
import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-list-component',
  templateUrl: './list-component.component.html',
  styleUrls: ['./list-component.component.scss']
})
export class ListComponentComponent implements OnInit {
  @Input() reserve : any;
  constructor(private router : Router, private reserveService : ReservesService) { }

  ngOnInit(): void {
  }

  // Pass reserve data to edit form.
  navToForm(id){
    this.router.navigate(['getreserve', id])
  }

  deleteReserve(id) {
    this.reserveService.deleteReserve(id).subscribe((data: any[]) => {
      console.log(data);
      window.location.reload();
    });

  }

}
