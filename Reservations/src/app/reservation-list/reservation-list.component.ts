import { ReservesService } from './../reserves.service';
import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit {

  constructor(private ReservesService : ReservesService) { }
  data : any;
  collectionSize: number;
  pageSize: number = 10;
  page: number = 1;
  action : number = 1;

  ngOnInit(): void {
    this.getdata();
  }
  getdata() {
    this.ReservesService.getData().subscribe((data: any[]) => {
      this.data = data['data'];
      this.sortData(1);

    })
  }

  sortData(action) {
       switch(action) {
        case 1: {
          console.log(this.data);
          this.data = _.orderBy(this.data,['dateofReserve'],['asc']);
          console.log(this.data);
          break;
        }
        case 2: {
          console.log(this.data);
          this.data = _.orderBy(this.data,['dateofReserve'],['desc']);
          console.log(this.data);
          break;
        }
        case 3: {
          console.log(this.data);
          this.data = _.orderBy(this.data,['place.name'],['asc']);
          console.log(this.data);
          break;
        }
        case 4: {
          console.log(this.data);
          this.data = _.orderBy(this.data,['place.name'],['desc']);
          console.log(this.data);
          break;
        }
        case 5: {
          console.log(this.data);
          this.data = _.orderBy(this.data,['place.rating'],['desc']);
          console.log(this.data);
          break;
        }
    }
  }
}

