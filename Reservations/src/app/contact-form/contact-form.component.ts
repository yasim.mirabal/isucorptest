import { Router } from '@angular/router';
import { ReservesService } from './../reserves.service';
import { Component, OnInit, AfterViewInit} from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { observable, Observable } from 'rxjs';
import {fromEvent} from 'rxjs';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  model: NgbDateStruct;
  minDate = { year: 1985, month: 1, day: 1 };
  maxDate={year:new Date().getFullYear(),month: 1, day: 1}
  startDate = { year: 1988, month: 1, day: 1 };
  public Editor = ClassicEditor;
  contact: any
  type: any = "ContactType"


  constructor(private ReservesService : ReservesService, private router : Router) { }

  // If contact already exists just navigate to Reserve Form. Else post contact data.
  onSubmit(data, form) {
    if (this.contact) {
      form.reset();
      this.router.navigate(['addreserve', this.contact.id])
    }
    else {
      this.ReservesService.postData(data).subscribe((result) => {
          form.reset();
          this.router.navigate(['addreserve', result['data']])
      });
    }
  }

  ngOnInit(): void {
  }
  // For autocomplete if contact exists.
  getByName() {
    const input:any = document.getElementById('search');
    this.ReservesService.getContactByName(input.value).subscribe((res) => {
      this.contact = res['data'];
      this.type = this.type + this.contact.type;
      var date = new Date(this.contact.birthDate);
      var ngbDateStruct = { day: date.getUTCDate(), month: date.getMonth() + 1, year: date.getFullYear()};
      this.model = ngbDateStruct;
    })
  }

}
