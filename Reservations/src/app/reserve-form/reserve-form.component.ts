import { PlaceServiceService } from './../place-service.service';
import { Component, OnInit } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { ReservesService } from '../reserves.service';


@Component({
  selector: 'app-reserve-form',
  templateUrl: './reserve-form.component.html',
  styleUrls: ['./reserve-form.component.scss']
})
export class ReserveFormComponent implements OnInit {
  dataPlace: any;
  dataReserve: any;
  date: any;
  model: NgbDateStruct;
  minDate = { year: new Date().getFullYear(), month: new Date().getMonth(), day: new Date().getDate() };
  contactId : number;
  reserveId : number;

  constructor(private route: ActivatedRoute, private router : Router, private service : PlaceServiceService, private reserveService : ReservesService) { }

  ngOnInit(): void {
    this.getdata();
    if (this.reserveId = Number(this.route.snapshot.paramMap.get('idReserve'))){
      this.getOne(Number(this.route.snapshot.paramMap.get('idReserve')));
    }
  }

  getdata() {
    this.service.getData().subscribe((data: any[]) => {
      this.dataPlace = data['data'];
    })
  }


  // Get Reserve data for edit, format Date for ngbDateStruct compatibility.
  getOne(id) {
    this.reserveService.getOneReserve(id).subscribe((data: any[]) => {
      this.dataReserve = data['data'];
      console.log(this.dataReserve);
      var date = new Date(this.dataReserve.dateofReserve);
      var ngbDateStruct = { day: date.getUTCDate(), month: date.getMonth(), year: date.getFullYear()};
      this.model = ngbDateStruct;
    });
  }

  // Submit for both, update and new reserve.

  onSubmit(reserve, form){
    if (this.dataReserve) {
      this.dataReserve.dateofReserve = this.model;
      console.log(this.dataReserve);
      this.reserveService.putreserveData(this.dataReserve).subscribe((result) => {
      this.router.navigate([''])
      })
    }
    else {
      this.contactId = Number(this.route.snapshot.paramMap.get('id'));
      reserve.ContactId = this.contactId;
      this.reserveService.postreserveData(reserve).subscribe((result) => {
        form.reset();
        this.router.navigate([''])
      })
    }
  }
}
