import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.scss']
})
export class HeaderNavComponent implements OnInit {

  btn: string;

  constructor(private router : Router) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
      var splitted = window.location.href.split("/");
      this.btn = splitted[3];
      }
    })
   }

  ngOnInit(): void {
    var splitted = window.location.href.split("/");
    this.btn = splitted[3];

  }

}
