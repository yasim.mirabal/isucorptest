import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ReservationListComponent } from './reservation-list/reservation-list.component';
import { ListComponentComponent } from './list-component/list-component.component';
import { NgbModule, NgbPagination, NgbPaginationModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import {ReservesService} from './reserves.service';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ReserveFormComponent } from './reserve-form/reserve-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderNavComponent,
    NavbarComponent,
    ReservationListComponent,
    ListComponentComponent,
    ContactFormComponent,
    ReserveFormComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    CKEditorModule,
    NgbDropdownModule
  ],
  providers: [ReservesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
