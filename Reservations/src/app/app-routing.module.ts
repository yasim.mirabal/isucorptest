import { ReserveFormComponent } from './reserve-form/reserve-form.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ContactFormComponent} from './contact-form/contact-form.component';
import {ReservationListComponent} from './reservation-list/reservation-list.component';


const routes: Routes = [
  {
    component: ContactFormComponent,
    path: 'addcontact'
  },
  {
    component: ReservationListComponent,
    path: ''
  },
  {
    component: ReserveFormComponent,
    path: 'addreserve/:id'
  },
  {
    component: ReserveFormComponent,
    path: 'getreserve/:idReserve'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
