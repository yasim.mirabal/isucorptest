import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders }    from '@angular/common/http';
import { ConditionalExpr } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class PlaceServiceService {
  constructor(private http: HttpClient) { }
  headers = new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
  endpoint : string = 'http://localhost:5000/';

  getData(){
    return this.http.get(this.endpoint + 'place/getall');
  }
  getById(id){
    return this.http.post(this.endpoint + 'place', id);
  }

}

