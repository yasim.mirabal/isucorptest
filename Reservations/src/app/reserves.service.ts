import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders }    from '@angular/common/http';
import { ConditionalExpr } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class ReservesService {
  constructor(private http: HttpClient) { }
  headers = new HttpHeaders({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'})
  endpoint : string = 'http://localhost:5000/';

  getData(){
    return this.http.get(this.endpoint + 'reserve/getall');
  }
  postData(formData){
    console.log(formData.BirthDate['year']);
    formData.BirthDate = new Date(formData.BirthDate['year'], formData.BirthDate['month'], formData.BirthDate['day']);
    console.log(formData.BirthDate);
    return this.http.post(this.endpoint + 'contact', formData);
  }
  postreserveData(formData) {
    formData.DateofReserve = new Date(formData.DateofReserve['year'], formData.DateofReserve['month'], formData.DateofReserve['day']);
    return this.http.post(this.endpoint + 'reserve', formData);
  }
  getOneReserve(id) {
    return this.http.get(this.endpoint + 'reserve/' + id);
  }

  getContactByName(name) {
    return this.http.get(this.endpoint + 'contact/' + name)
  }

  deleteReserve(id) {
    return this.http.delete(this.endpoint + 'reserve/' + id);
  }

  putreserveData(formData) {
    formData.DateofReserve = new Date(formData.dateofReserve.year, formData.dateofReserve.month -1, formData.dateofReserve.day);
    formData.dateofReserve = formData.DateofReserve.getFullYear() + "/" + (formData.DateofReserve.getMonth() + 1) + "/" + formData.DateofReserve.getDate()
    formData.PlaceId = formData['place'].id;
    console.log(formData);
    return this.http.put(this.endpoint + 'reserve', formData);
  }

}
