﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Reservation.Migrations
{
    public partial class ReserveModelUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Reserves");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Reserves",
                type: "text",
                nullable: true);
        }
    }
}
