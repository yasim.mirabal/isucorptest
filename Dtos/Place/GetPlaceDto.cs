namespace Reservation.Dtos.PlaceDtos
{
    public class GetPlaceDto
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public float Rating {get; set;}
        public bool Favorite {get; set;}
        public string Image {get; set;}
    }
}