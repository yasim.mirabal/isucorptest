using System;

namespace Reservation.Dtos.Reserve
{
    public class UpdateReserveDto
    {
        public int Id {get; set;}
        public DateTime DateofReserve {get; set;}
        public int ContactId {get; set;}
        public int PlaceId {get; set;}
    }
}