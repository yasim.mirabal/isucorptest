using System;
using Reservation.Models;

namespace Reservation.Dtos.Reserve
{
    public class GetReserveDto
    {
        public int Id {get; set;}
        public DateTime DateofReserve {get; set;}
        public Contact Contact {get; set;}
        public Place Place {get; set;}
    }
}