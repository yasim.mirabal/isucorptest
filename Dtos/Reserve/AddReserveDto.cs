using System;

namespace Reservation.Dtos.Reserve
{
    public class AddReserveDto
    {
        public DateTime DateofReserve {get; set;}
        public int ContactId {get; set;}
        public int PlaceId {get; set;}
    }
}