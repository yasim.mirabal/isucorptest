using AutoMapper;
using Reservation.Dtos.ContactDtos;
using Reservation.Dtos.PlaceDtos;
using Reservation.Dtos.Reserve;
using Reservation.Models;

namespace Reservation
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Contact, GetContactDto>();
            CreateMap<AddContactDto, Contact>();
            CreateMap<UpdateContactDto, Contact>();
            CreateMap<Reserve, GetReserveDto>();
            CreateMap<AddReserveDto, Reserve>();
            CreateMap<UpdateReserveDto, Reserve>();
            CreateMap<Place, GetPlaceDto>();
        }
    }
}