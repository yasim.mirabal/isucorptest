using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Reservation.Services.PlaceService;

namespace Reservation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlaceController : ControllerBase
    {
        private readonly IPlaceService _placeService;
        public PlaceController(IPlaceService placeService)
        {
            _placeService = placeService;

        }
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _placeService.GetAllPlaces());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            return Ok(await _placeService.GetPlaceById(id));
        }
    }
}