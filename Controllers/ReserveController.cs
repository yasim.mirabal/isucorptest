using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Reservation.Dtos.Reserve;
using Reservation.Models;
using Reservation.Services.ReserveService;

namespace Reservation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReserveController : ControllerBase
    {
        private readonly IReserveService _ReserveService;
        public ReserveController(IReserveService ReserveService)
        {
            _ReserveService = ReserveService;

        }
       
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _ReserveService.GetAllReserves());
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSingle(int id)
        {
            return Ok(await _ReserveService.GetReserveById(id));
        }
        
        [HttpPost]
        public async Task<IActionResult> AddReserve(AddReserveDto newReserve, int contactId)
        {
            return Ok(await _ReserveService.AddReserve(newReserve));
        }
       
        [HttpPut]
        public async Task<IActionResult> UpdateReserve(UpdateReserveDto updatedReserve)
        {
            ServiceResponse<GetReserveDto> response = await _ReserveService.UpdateReserve(updatedReserve);
            if (response.Data == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }
        
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ServiceResponse<List<GetReserveDto>> response = await _ReserveService.DeleteReserve(id);
            if (response.Data == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }
    }
}