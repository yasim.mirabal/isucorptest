using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Reservation.Dtos.ContactDtos;
using Reservation.Models;
using Reservation.Services.ContactServices;

namespace Reservation.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactController : ControllerBase
    {
        private readonly IContactService _contactService;
        public ContactController(IContactService contactService)
        {
            _contactService = contactService;

        }
        [Route("GetAll")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _contactService.GetAllContacts());
        }

        [HttpGet("{name}")]
        public async Task<IActionResult> GetSingle(string name)
        {
            return Ok(await _contactService.GetContactByName(name));
        }

        [HttpPost]
        public async Task<IActionResult> AddContact(AddContactDto newcontact)
        {
            return Ok(await _contactService.AddContact(newcontact));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateContact(UpdateContactDto updatedContact)
        {
            ServiceResponse<GetContactDto> response = await _contactService.UpdateContact(updatedContact);
            if (response.Data == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ServiceResponse<List<GetContactDto>> response = await _contactService.DeleteContact(id);
            if (response.Data == null)
            {
                return NotFound(response);
            }

            return Ok(response);
        }
    }
}