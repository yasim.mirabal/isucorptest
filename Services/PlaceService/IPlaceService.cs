using System.Collections.Generic;
using System.Threading.Tasks;
using Reservation.Dtos.PlaceDtos;
using Reservation.Models;

namespace Reservation.Services.PlaceService
{
    public interface IPlaceService
    {
         Task<ServiceResponse<List<GetPlaceDto>>> GetAllPlaces();
         Task<ServiceResponse<GetPlaceDto>> GetPlaceById(int id);
    }
}