using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Reservation.Data;
using Reservation.Dtos.PlaceDtos;
using Reservation.Models;

namespace Reservation.Services.PlaceService
{
    public class PlaceService : IPlaceService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        public PlaceService(IMapper mapper, DataContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<ServiceResponse<List<GetPlaceDto>>> GetAllPlaces()
        {
            ServiceResponse<List<GetPlaceDto>> serviceResponse = new ServiceResponse<List<GetPlaceDto>>();
            List<Place> dbPlaces = await _context.Places.ToListAsync();
            serviceResponse.Data = dbPlaces.Select(c => _mapper.Map<GetPlaceDto>(c)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<GetPlaceDto>> GetPlaceById(int id)
        {
            ServiceResponse<GetPlaceDto> serviceResponse = new ServiceResponse<GetPlaceDto>();
            Place dbPlaces = await _context.Places.FirstOrDefaultAsync(c => c.Id == id);
            serviceResponse.Data = _mapper.Map<GetPlaceDto>(dbPlaces);
            return serviceResponse;
        }
    }
}