using System.Collections.Generic;
using System.Threading.Tasks;
using Reservation.Dtos.ContactDtos;
using Reservation.Models;

namespace Reservation.Services.ContactServices
{
    public interface IContactService
    {
         Task<ServiceResponse<List<GetContactDto>>> GetAllContacts();
         Task<ServiceResponse<GetContactDto>> GetContactByName(string name);
         Task<ServiceResponse<int>> AddContact(AddContactDto newcontact);
         Task<ServiceResponse<GetContactDto>> UpdateContact(UpdateContactDto updatedContact);
         Task<ServiceResponse<List<GetContactDto>>> DeleteContact(int id);

    }
}