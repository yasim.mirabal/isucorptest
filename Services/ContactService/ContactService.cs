using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Reservation.Data;
using Reservation.Dtos.ContactDtos;
using Reservation.Models;

namespace Reservation.Services.ContactServices
{
    public class ContactService : IContactService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;

        public ContactService(IMapper mapper, DataContext context)
        {
            _mapper = mapper;
            _context = context;
        }
        public async Task<ServiceResponse<int>> AddContact(AddContactDto newcontact)
        {
            ServiceResponse<int> serviceResponse = new ServiceResponse<int>();
            Contact contact = _mapper.Map<Contact>(newcontact);
            await _context.Contacts.AddAsync(contact);
            await _context.SaveChangesAsync();
            int id = contact.Id;
            serviceResponse.Data = id;
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetContactDto>>> DeleteContact(int id)
        {
            ServiceResponse<List<GetContactDto>> serviceResponse = new ServiceResponse<List<GetContactDto>>();
            try
            {
                Contact contact = await _context.Contacts.FirstAsync(c => c.Id == id);
                _context.Contacts.Remove(contact);
                await _context.SaveChangesAsync();
                serviceResponse.Data = _context.Contacts.Select(c => _mapper.Map<GetContactDto>(c)).ToList();
            }
            catch
            {
                serviceResponse.Success = false;
                serviceResponse.Message = "No existe el contacto.";
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetContactDto>>> GetAllContacts()
        {
            ServiceResponse<List<GetContactDto>> serviceResponse = new ServiceResponse<List<GetContactDto>>();
            List<Contact> dbContacts = await _context.Contacts.ToListAsync();
            serviceResponse.Data = dbContacts.Select(c => _mapper.Map<GetContactDto>(c)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<GetContactDto>> GetContactByName(string name)
        {
            ServiceResponse<GetContactDto> serviceResponse = new ServiceResponse<GetContactDto>();
            Contact dbcontact = await _context.Contacts.FirstOrDefaultAsync(c => c.FullName == name);
            serviceResponse.Data = _mapper.Map<GetContactDto>(dbcontact);
            return serviceResponse;
        }
        public async Task<ServiceResponse<GetContactDto>> UpdateContact(UpdateContactDto updatedContact)
        {
            ServiceResponse<GetContactDto> serviceResponse = new ServiceResponse<GetContactDto>();
            try
            {
                Contact contact = _context.Contacts.FirstOrDefault(c => c.Id == updatedContact.Id);
                contact.FullName = updatedContact.FullName;
                contact.BirthDate = updatedContact.BirthDate;
                contact.Phone = updatedContact.Phone;
                contact.Type = updatedContact.Type;
                contact.Description = updatedContact.Description;
                _context.Contacts.Update(contact);
                await _context.SaveChangesAsync();
                serviceResponse.Data = _mapper.Map<GetContactDto>(contact);
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }

            return serviceResponse;
        }
    }
}