using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Reservation.Data;
using Reservation.Dtos.Reserve;
using Reservation.Models;

namespace Reservation.Services.ReserveService
{
    public class ReserveService : IReserveService
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;

        public ReserveService(IMapper mapper, DataContext context)
        {
            _mapper = mapper;
            _context = context;
        }
        public async Task<ServiceResponse<List<GetReserveDto>>> AddReserve(AddReserveDto newReserve)
        {
            ServiceResponse<List<GetReserveDto>> serviceResponse = new ServiceResponse<List<GetReserveDto>>();
            Reserve reserve = _mapper.Map<Reserve>(newReserve);
            reserve.Contact = await _context.Contacts.FirstOrDefaultAsync(c => c.Id == newReserve.ContactId);
            reserve.Place = await _context.Places.FirstOrDefaultAsync(p => p.Id == newReserve.PlaceId);
            await _context.Reserves.AddAsync(reserve);
            await _context.SaveChangesAsync();
            serviceResponse.Data = _context.Reserves.Select(r => _mapper.Map<GetReserveDto>(r)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetReserveDto>>> DeleteReserve(int id)
        {
            ServiceResponse<List<GetReserveDto>> serviceResponse = new ServiceResponse<List<GetReserveDto>>();
            try
            {
                Reserve reserve = await _context.Reserves.FirstAsync(r => r.Id == id);
                _context.Reserves.Remove(reserve);
                await _context.SaveChangesAsync();
                serviceResponse.Data = _context.Reserves.Select(r => _mapper.Map<GetReserveDto>(r)).ToList();
            }
            catch
            {
                serviceResponse.Success = false;
                serviceResponse.Message = "No existe el Reserveo.";
            }

            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetReserveDto>>> GetAllReserves()
        {
            ServiceResponse<List<GetReserveDto>> serviceResponse = new ServiceResponse<List<GetReserveDto>>();
            List<Reserve> dbReserves = await _context.Reserves.Include(c => c.Contact).Include(p => p.Place).ToListAsync();
            serviceResponse.Data = dbReserves.Select(r => _mapper.Map<GetReserveDto>(r)).ToList();
            return serviceResponse;
        }

        public async Task<ServiceResponse<GetReserveDto>> GetReserveById(int id)
        {
            ServiceResponse<GetReserveDto> serviceResponse = new ServiceResponse<GetReserveDto>();
            Reserve dbReserve = await _context.Reserves.Include(c => c.Contact).Include(p => p.Place).FirstOrDefaultAsync(r => r.Id == id);
            serviceResponse.Data = _mapper.Map<GetReserveDto>(dbReserve);
            return serviceResponse;
        }
        public async Task<ServiceResponse<GetReserveDto>> UpdateReserve(UpdateReserveDto updatedReserve)
        {
            ServiceResponse<GetReserveDto> serviceResponse = new ServiceResponse<GetReserveDto>();
            try
            {
                Reserve reserve = _context.Reserves.FirstOrDefault(r => r.Id == updatedReserve.Id);
                reserve.DateofReserve = updatedReserve.DateofReserve;
                reserve.Place = await _context.Places.FirstOrDefaultAsync(p => p.Id == updatedReserve.PlaceId);
                _context.Reserves.Update(reserve);
                await _context.SaveChangesAsync();
                serviceResponse.Data = _mapper.Map<GetReserveDto>(reserve);
            }
            catch (Exception ex)
            {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }

            return serviceResponse;
        }
    }
}