using System.Collections.Generic;
using System.Threading.Tasks;
using Reservation.Dtos.Reserve;
using Reservation.Models;

namespace Reservation.Services.ReserveService
{
    public interface IReserveService
    {
         Task<ServiceResponse<List<GetReserveDto>>> GetAllReserves();
         Task<ServiceResponse<GetReserveDto>> GetReserveById(int id);
         Task<ServiceResponse<List<GetReserveDto>>> AddReserve(AddReserveDto newReserve);
         Task<ServiceResponse<GetReserveDto>> UpdateReserve(UpdateReserveDto updatedReserve);
         Task<ServiceResponse<List<GetReserveDto>>> DeleteReserve(int id);
    }
}